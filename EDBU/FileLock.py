import os
import fcntl

class FileLock(object):
    def __init__(self, filename):
        self.locked = False
        self.filename = filename
        self.handle = None

    def acquire(self):
        if self.locked:
            if self.handle is None or self.handle.closed is True:
                raise RuntimeError('The lock file is accuired but closed: {}'.format(self.filename))
            return self.handle
        self.handle = open(self.filename, 'w')
        fcntl.flock(self.handle, fcntl.LOCK_EX|fcntl.LOCK_NB)
        self.locked = True
        return self.handle

    def release(self):
        if not self.locked:
            return
        if self.handle is None or self.handle.closed is True:
            raise RuntimeError(
                'Try to release an uninitialized or already released FileLock: {}' \
                .format(self.filename))
        fcntl.flock(self.handle, fcntl.LOCK_UN|fcntl.LOCK_NB)
        self.handle.close()
        os.remove(self.filename)
        self.locked = False

    def __enter__(self):
        return self.acquire()

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.release()

    def __del__(self):
        self.release()
