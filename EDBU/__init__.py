__author__ = 'Adam Cavendish'
__version__ = '0.0.1'

from .utility import *
from .models import Energy, Extremum
from .ETreeCreator import ETreeCreator
from .FileLock import FileLock

UPLOAD_NOT_YET = 0
UPLOAD_SUCCESSFUL = 1
UPLOAD_TIMEOUT = -1
UPLOAD_DATA_ERROR = -2
