import collections

def get_or_unwrap(dct, key, default=None):
    value = dct.get(key)
    if value is not None:
        return value
    if default is not None:
        print('Warning: Using default value ({}={})'.format(key, default))
        return default
    else:
        print('Error: No value for "{}"'.format(key))
        raise RuntimeError('No value for "{}"'.format(key))

class BatchMap(object):
    '''
    Use append/popleft of deque to extract batch cache objects out of iterable
    '''
    def __init__(self, batch_size, iterable):
        self.batch_size = batch_size
        self.iterable = iterable
        self.cache = collections.deque(maxlen=max(2, self.batch_size+1))
        self.__fill_batch()

    def __iter__(self):
        return self

    def __next__(self):
        try:
            element = self.cache.popleft()
        except:
            raise StopIteration

        try:
            dat = next(self.iterable)
            self.cache.append(dat)
        except StopIteration:
            pass

        return element

    def __fill_batch(self):
        fill_size = max(0, self.cache.maxlen-len(self.cache)-1)
        try:
            for _ in range(fill_size):
                dat = next(self.iterable)
                self.cache.append(dat)
        except StopIteration:
            pass
