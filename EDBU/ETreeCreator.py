'''
Database ORM Models
'''

import lxml
import lxml.etree

class ETreeCreator(object):
    '''
    ETreeCreator utility class for creating tree-structured XML elements
    '''

    @staticmethod
    def pretty(element):
        return lxml.etree.tostring(
            element, pretty_print=True, xml_declaration=True,
            encoding='UTF-8').decode()

    @staticmethod
    def tostring(element):
        return lxml.etree.tostring(
            element, pretty_print=False, xml_declaration=True,
            encoding='UTF-8').decode()

    @staticmethod
    def flatten(lst):
        return sum(([x] if not isinstance(x, list)
                    else ETreeCreator.flatten(x) for x in lst), [])

    @staticmethod
    def element(tag_name, attrs=None, text=None):
        '''
        parameters:
        - tag_name: str
        - attrs: [(str,str)] => [('key','value')]
        - text: str
        return:
        - lxml.etree.Element
        '''
        elm = lxml.etree.Element(tag_name)
        if attrs is not None:
            for attr in attrs:
                elm.set(attr[0], attr[1])
        if text is not None:
            elm.text = text
        return elm

    @staticmethod
    def compose(elem, subelems):
        '''
        parameters:
        - elem: lxml.etree.Element
        - subelems: [lxml.etree.Element]
        return:
        - lxml.etree.Element
        '''
        for subelem in subelems:
            elem.append(subelem)
        return elem

    @staticmethod
    def compose_flatten(elem, subelems):
        '''
        parameters:
        - elem: lxml.etree.Element
        - subelems: [lxml.etree.Element]
        return:
        - lxml.etree.Element
        '''
        return ETreeCreator.compose(elem, ETreeCreator.flatten(subelems))
