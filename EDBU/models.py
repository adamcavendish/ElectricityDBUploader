'''
ORM Models for 'upload_energy' table and 'upload_m' table
'''

from sqlalchemy import Column, Integer, DateTime, String
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class Energy(Base):
    '''Energy model'''
    __tablename__ = 'upload_energy'
    tId = Column('tId', Integer, primary_key=True)
    devId = Column('devId', Integer)
    pTime = Column('pTime', DateTime)
    pType = Column('pType', Integer)
    pValue = Column('pValue', String)
    upload = Column('upload', Integer)

    def __repr__(self):
        return '<{},{},{},{},{},{}>'.format(
            self.tId, self.devId+1000, self.pTime.strftime('%Y%m%d%H%M%S'),
            self.pType, self.pValue, self.upload)

class Extremum(Base):
    '''Extremum model'''
    __tablename__ = 'upload_m'
    tId = Column('tId', Integer, primary_key=True)
    devId = Column('devId', Integer)
    mTime = Column('mTime', DateTime)
    mType = Column('mType', Integer)
    maxValue = Column('maxValue', String)
    maxTime = Column('maxTime', DateTime)
    minValue = Column('minValue', String)
    minTime = Column('minTime', DateTime)
    vAvg = Column('vAvg', String)
    upload = Column('upload', Integer)

    def __repr__(self):
        return '<{},{},{},{},{},{},{},{},{},{}>'.format(
            self.tId, self.devId+1000,
            self.mTime.strftime('%Y%m%d%H%M%S'), self.mType,
            self.maxValue, self.maxTime.strftime('%H%M%S'),
            self.minValue, self.minTime.strftime('%H%M%S'),
            self.vAvg, self.upload)
