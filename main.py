'''
Main entry
'''

import concurrent.futures
import json
import os
import sys

import lxml
import zeep
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import scoped_session

from EDBU import *

def gen_enterprise_registration_tree(secret, enterprise_id):
    '''
    paramter:
    - enterprise_id: str
    return:
    - lxml.etree.Element
    '''
    e = lambda tag_name, content=None: ETreeCreator.element(tag_name, None, content)
    c = ETreeCreator.compose

    reqtype = '01'

    return c(e('request'), [
        e('reqtype', reqtype),
        e('secret', secret),
        e('body', enterprise_id)
    ])


def gen_all_data_tree(secret, userid, seeid, time, type_values):
    '''
    parameter:
    - secret:  str
    - userid:  str
    - seeid:   str
    - time:    str
    - type_values: [(str,str)] => [('type','value')]
    return:
    - lxml.etree.Element
    '''
    e = lambda tag_name, content=None: ETreeCreator.element(tag_name, None, content)
    c = ETreeCreator.compose
    cf = ETreeCreator.compose_flatten

    reqtype = '05'

    def gen_type_values(type_values):
        '''
        parameter:
        - type_values: [(str,str)] => [('type','value')]
        return:
        - [lxml.etree.Element]
        '''
        return [
            c(e('data'), [
                e('type', cur_type),
                e('value', cur_val)
            ])
            for cur_type, cur_val in type_values]

    return c(e('request'), [
        e('reqtype', reqtype),
        e('secret', secret),
        cf(e('body'), [
            e('userid', userid),
            e('seeid', seeid),
            e('time', time),
            gen_type_values(type_values)
        ])
    ])


def gen_extremum_data_tree(secret, userid, seeid, time, values):
    '''
    parameter:
    - secret:  str
    - userid:  str
    - seeid:   str
    - time:    str
    - values: [(   str,       str,      str,       str,      str,  str)]
           => [('type','maxvalue','maxtime','minvalue','mintime','avg')]
    return:
    - lxml.etree.Element
    '''
    e = lambda tag_name, content=None: ETreeCreator.element(tag_name, None, content)
    c = ETreeCreator.compose
    cf = ETreeCreator.compose_flatten

    reqtype = '10'

    def gen_type_values(values):
        '''
        parameter:
        - values: [(   str,       str,      str,       str,      str,  str)]
               => [('type','maxvalue','maxtime','minvalue','mintime','avg')]
        return:
        - [lxml.etree.Element]
        '''
        type_value_tuples = [
            tuple(zip(('type', 'maxvalue', 'maxtime', 'minvalue', 'mintime', 'avg'),
                      five_vals))
            for five_vals in values]
        return [c(e('data'),
                  [e(tag_name, tag_content) for tag_name, tag_content in tv_tuple])
                for tv_tuple in type_value_tuples]

    return c(e('request'), [
        e('reqtype', reqtype),
        e('secret', secret),
        cf(e('body'), [
            e('userid', userid),
            e('seeid', seeid),
            e('time', time),
            gen_type_values(values)
        ])
    ])


def gen_energy_data(userid, energy_lst, devId_str, pTime_str):
    '''
    parameter:
    - userid: str
    - energy_lst: Energy model list
    - devId_str: the devId in string (note: should +1000)
    - pTime_str: the pTime in string (note: format strftime('%Y%m%d%H%M%S'))
    return:
    - lxml.etree.Element
    '''
    if not hasattr(energy_lst, '__next__') and not hasattr(energy_lst, '__iter__'):
        raise RuntimeError("gen_energy_data needs a list of energies")

    type_values = map(lambda cur_e: (str(cur_e.pType), str(cur_e.pValue)), energy_lst)
    return gen_all_data_tree('', userid, devId_str, pTime_str, type_values)


def gen_extremum_data(userid, extremum):
    '''
    parameter:
    - userid: str
    - extremum: Extremum model
    return:
    - lxml.etree.Element
    '''
    return gen_extremum_data_tree('', userid, str(extremum.devId+1000),
                                  extremum.mTime.strftime('%Y%m%d%H%M%S'),
                                  [(str(extremum.mType),
                                    str(extremum.maxValue),
                                    extremum.maxTime.strftime('%H%M%S'),
                                    str(extremum.minValue),
                                    extremum.minTime.strftime('%H%M%S'),
                                    str(extremum.vAvg))])


def enterprise_registration(client, data):
    ret = client.service.enterpriseRegistration(data)
    root = lxml.etree.fromstring(ret.encode())
    flags = root.xpath('/response/flag/text()')
    user_ids = root.xpath('/response/body/text()')
    if len(flags) != 1:
        raise RuntimeError('Unexpected Enterprise Registration Return: {}'.format(ret))
    if flags[0] == '1' and len(user_ids) != 1:
        raise RuntimeError('Unexpected Enterprise Registration Return: {}'.format(ret))
    return (flags[0], user_id) if flags[0] == 1 else (0, None)


def upload_all_data(client, data):
    try:
        ret = client.service.uploadAllData(data)
        root = lxml.etree.fromstring(ret.encode())
        flags = root.xpath('/response/flag/text()')
        if len(flags) != 1:
            raise RuntimeError('Unexpected Upload All Data Return: {}'.format(ret))
        return flags[0]
    except Exception as e:
        print('upload_all_data error: {}'.format(e))
        print('                 data: {}'.format(data))
        # Return that it is an error
        return 0


def upload_extremum_data(client, data):
    try:
        ret = client.service.uploadMostData(data)
        root = lxml.etree.fromstring(ret.encode())
        flags = root.xpath('/response/flag/text()')
        if len(flags) != 1:
            raise RuntimeError('Unexpected Upload All Data Return: {}'.format(ret))
        return flags[0]
    except Exception as e:
        print('upload_extremum_data error: {}'.format(e))
        print('                      data: {}'.format(data))
        # Return that it is an error
        return 0


def upload_all_data_from_database(Session, max_workers, me_record_limit, debug_mode):
    def upload_one(energy_lst, devId_str, pTime_str):
        energy_el = gen_energy_data(enterprise_id, energy_lst, devId_str, pTime_str)
        energy_xml = ETreeCreator.tostring(energy_el)
        if debug_mode is True:
            print('Upload content:')
            print(energy_xml)
            print('Prettify:')
            print(ETreeCreator.pretty(energy_el))
        return upload_all_data(elec_client, energy_xml)

    def gen_upload_energy_lst(session, energy_devId_time_not_uploaded):
        for pair in energy_devId_time_not_uploaded:
            energy_devId = pair[0]
            energy_pTime = pair[1]
            energy_lst = session.query(Energy) \
                .filter(Energy.upload.in_([UPLOAD_NOT_YET, UPLOAD_TIMEOUT, UPLOAD_DATA_ERROR])) \
                .filter(Energy.devId == energy_devId, Energy.pTime == energy_pTime).all()
            yield (energy_lst, str(energy_devId + 1000), energy_pTime.strftime('%Y%m%d%H%M%S'))

    query_session = Session()
    energy_devId_time_not_uploaded = query_session.query(Energy.devId, Energy.pTime) \
                                        .filter(Energy.upload.in_(
                                            [UPLOAD_NOT_YET, UPLOAD_TIMEOUT, UPLOAD_DATA_ERROR])) \
                                        .group_by(Energy.devId, Energy.pTime).all()
    energy_count = len(energy_devId_time_not_uploaded)

    executor = concurrent.futures.ThreadPoolExecutor(max_workers=max_workers)

    # Decide whether to use memory efficient upload method
    if energy_count > me_record_limit:
        print('Upload Energy {} records: Using Memory Efficient Uploading' \
                .format(energy_count))
        upload_futures = \
                BatchMap(max_workers,
                         map(lambda energy_info:
                             (energy_info[0], executor.submit(upload_one, *energy_info)),
                             gen_upload_energy_lst(query_session, energy_devId_time_not_uploaded)))
    else:
        print('Upload Energy {} records: Using Fast Uploading'.format(energy_count))
        upload_futures = \
                [(energy_info[0], executor.submit(upload_one, *energy_info))
                 for energy_info in
                 gen_upload_energy_lst(query_session, energy_devId_time_not_uploaded)]

    query_session.close()

    update_session = Session()
    for idx, tpl in enumerate(upload_futures):
        energy_lst = tpl[0]
        future = tpl[1]
        try:
            flag = future.result(timeout=upload_request_timeout)
            if flag == '1':
                for energy in energy_lst:
                    print('Upload Energy tid={}, Success!'.format(energy.tId))
                    energy.upload = UPLOAD_SUCCESSFUL
                    update_session.add(energy)
            else:
                for energy in energy_lst:
                    print('Upload Energy tid={}, Data Error: {}'.format(energy.tId, str(energy)))
                    energy.upload = UPLOAD_DATA_ERROR
                    update_session.add(energy)
        except concurrent.futures.TimeoutError:
            future.cancel()
            for energy in energy_lst:
                print('Upload Energy tid={}, Timeout!'.format(energy.tId))
                energy.upload = UPLOAD_TIMEOUT
                update_session.add(energy)
        if idx % max_workers == max_workers-1:
            update_session.commit()
    update_session.commit()
    update_session.close()

    executor.shutdown(wait=False)


def upload_extremum_data_from_database(Session, max_workers, me_record_limit, debug_mode):
    def upload_one(extremum):
        extremum_el = gen_extremum_data(enterprise_id, extremum)
        extremum_xml = ETreeCreator.tostring(extremum_el)
        if debug_mode is True:
            print('Upload content:')
            print(extremum_xml)
            print('Prettify:')
            print(ETreeCreator.pretty(extremum_el))
        return upload_extremum_data(elec_client, extremum_xml)

    query_session = Session()
    extremum_count = query_session.query(Extremum) \
                    .filter(
                        Extremum.upload.in_([UPLOAD_NOT_YET, UPLOAD_TIMEOUT, UPLOAD_DATA_ERROR])) \
                    .count()
    extremum_data = query_session.query(Extremum) \
                    .filter(
                        Extremum.upload.in_([UPLOAD_NOT_YET, UPLOAD_TIMEOUT, UPLOAD_DATA_ERROR]))

    executor = concurrent.futures.ThreadPoolExecutor(max_workers=max_workers)

    # Decide whether to use memory efficient upload method
    if extremum_count > me_record_limit:
        print('Upload Extremum {} records: Using Memory Efficient Uploading' \
                .format(extremum_count))
        upload_futures = \
                BatchMap(max_workers,
                         map(lambda extremum: (extremum, executor.submit(upload_one, extremum)),
                             extremum_data))
    else:
        print('Upload Extremum {} records: Using Fast Uploading'.format(extremum_count))
        upload_futures = \
                [(extremum, executor.submit(upload_one, extremum)) for extremum in extremum_data]

    query_session.close()

    update_session = Session()
    for idx, tpl in enumerate(upload_futures):
        extremum = tpl[0]
        future = tpl[1]
        try:
            flag = future.result(timeout=upload_request_timeout)
            if flag == '1':
                print('Upload Extremum tid={}, Success!'.format(extremum.tId))
                extremum.upload = UPLOAD_SUCCESSFUL
            else:
                print('Upload Extremum tid={}, Data Error: {}'.format(extremum.tId, str(extremum)))
                extremum.upload = UPLOAD_DATA_ERROR
        except concurrent.futures.TimeoutError:
            future.cancel()
            print('Upload Extremum tid={}, Timeout!'.format(extremum.tId))
            extremum.upload = UPLOAD_TIMEOUT
        update_session.add(extremum)
        if idx % max_workers == max_workers-1:
            update_session.commit()
    update_session.commit()
    update_session.close()

    executor.shutdown(wait=False)


if __name__ == '__main__':
    CONFIG_FILE_NAME = 'EDBU_config.json'

    # Load configuration file
    EDBU_DIR = os.environ.get('EDBU_DIR')
    if os.path.isfile(CONFIG_FILE_NAME):
        EDBU_DIR = os.getcwd()

    if EDBU_DIR is None:
        error_msg = 'Use run.bash to run this script. ' \
            'Or manually setup EDBU_DIR environment variable'
        print(error_msg)
        raise RuntimeError(error_msg)

    with open(os.path.join(EDBU_DIR, CONFIG_FILE_NAME)) as f:
        EDBU_CONFIG = f.read()
    EDBU_CONFIG = json.loads(EDBU_CONFIG)

    # Check lock file
    try:
        lock_file_path = get_or_unwrap(EDBU_CONFIG, 'lock_file', '/tmp/EDBU.upload.lock')
        lock = FileLock(lock_file_path)
        try:
            lock.acquire()
        except BlockingIOError as e:
            print(e)
            print(
                'EDBU Uploader seems to be already running ... ' \
                'if not, please remove file `{}` manually!'
                .format(lock_file_path))
            sys.exit(1)

        # Connect MySQL and prepare for Zeep WSDL Interfaces
        mysql_engine = create_engine(
            'mysql://{db_user}:{db_passwd}@{db_hostname}:{db_port}/{db_dbname}' \
            .format(**EDBU_CONFIG))
        elec_client = zeep.Client(get_or_unwrap(EDBU_CONFIG, 'webservice_url'))
        # Load up some configs
        enterprise_id = get_or_unwrap(EDBU_CONFIG, 'enterprise_id')
        max_workers = get_or_unwrap(EDBU_CONFIG, 'max_threads', 10)
        me_record_limit = get_or_unwrap(EDBU_CONFIG, 'memory_efficient_limit', 5000)
        upload_request_timeout = get_or_unwrap(EDBU_CONFIG, 'upload_request_timeout', 4)
        debug_mode = get_or_unwrap(EDBU_CONFIG, 'debug_mode', False)

        if debug_mode is True:
            max_workers = 1

        # Generate a session
        DB_Session = sessionmaker(bind=mysql_engine, expire_on_commit=False)
        DB_Session = scoped_session(DB_Session)

        # Try Upload Energy Data
        upload_all_data_from_database(DB_Session, max_workers, me_record_limit, debug_mode)

        # Try Upload Exetremum Data
        upload_extremum_data_from_database(DB_Session, max_workers, me_record_limit, debug_mode)
    finally:
        lock.release()
