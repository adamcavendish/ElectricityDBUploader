# Electricity Database Uploader

## Brief

Python:
1. Use Python 3
2. Use [pyenv](https://github.com/yyuu/pyenv) for Python Version management. (Does not pollute your global python configurations)

Timing part:
1. Uploads electricity data every 15 minutes
2. Use 'crontab' for timing, and therefore works with both "System V" and "systemd"

Logging:
1. Logging to `current_directory/log/EDBU.log`
2. Database Table Column: upload
  * Normal States: `{"0": "Not yet uploaded", "1": "Upload successful"}`
  * Error States:  `{"-1": "Upload timeout", "-2": "Upload data error"}`

## Files

```
.
├── EDBU
│   ├── __init__.py            -- python package file
│   ├── FileLock.py
│   ├── utility.py
│   ├── main.py                -- ETreeCreator utility class, for creating XML tree
│   └── models.py              -- Database Model
├── EDBU_config.json           -- Configuration file
├── main.py                    -- Main entry
├── README.md                  -- Current file
└── shell
    ├── install.bash           -- Install crontab and python environments
    ├── requirements.txt       -- Python packages used by current project
    ├── run.bash               -- Script for running the project
    └── __run_setup_env__.bash -- Setup python environment helper script
```

### Install/Uninstall

Run this file and you'll have the script running every 15 minutes:

```
bash shell/install.bash
```

Uninstall instructions:

Step 1: Edit crontab

```
crontab -e
```

Step 2: Remove the line that matches the current project

# Configuration

```
{
  "db_user": "MySQL Database User",
  "db_passwd": "MySQL Database Password",
  "db_hostname": "MySQL Database hostname/ip",
  "db_port": "MySQL Database port (usually 3306)",
  "db_dbname": "Database Name",
  "enterprise_id": "Enterprise ID (500577)",
  "upload_request_timeout": "Every upload thread's execution timeout (default: 4)",
  "lock_file": "A file created and locked for interprocess identification. (default: '/tmp/EDBU.upload.lock')",
  "max_threads": "Maximum concurrent threads for uploading data. Note: too many threads might cause a clog in the scheduling queue, and thus false timeouts might be generated. (default: 10)",
  "memory_efficient_limit": "When records not uploaded size is greater than the limit, program will use a memory efficient way of uploading (default: 5000)",
  "debug_mode": "Enable debug mode will log the upload data and receive data, and use only one thread for uploading (default: false)",
  "webservice_url": "Webservice URL (Current Test URL is: http://114.215.157.224:8086/mds/services/DataService?wsdl)"
}
```

