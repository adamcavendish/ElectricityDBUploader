#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

export EDBU_DIR="$SCRIPT_DIR/../"

PYENV_ROOT="$HOME/.pyenv/"
export PATH="$PYENV_ROOT/bin:$PATH"
export C_INCLUDE_PATH="versions/3.5.2/include/:$C_INCLUDE_PATH"
export LD_LIBRARY_PATH="versions/3.5.2/lib/:$LD_LIBRARY_PATH"

export PYENV_VIRTUALENV_DISABLE_PROMPT=1

eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"

