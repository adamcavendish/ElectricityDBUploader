#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

. "$SCRIPT_DIR/__run_setup_env__.bash"

# Print timestamp
export TZ='Asia/Shanghai'
echo "Log time: $(date +"%Y-%m-%d@%H:%M:%S")"

# Run
pyenv activate EDBU_venv
python "$EDBU_DIR/main.py"

