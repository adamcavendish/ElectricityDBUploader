#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [ ! -d "$HOME/.pyenv/" ]; then
  git clone https://github.com/yyuu/pyenv.git "$HOME/.pyenv"
  git clone https://github.com/yyuu/pyenv-virtualenv.git "$HOME/.pyenv/plugins/pyenv-virtualenv"
fi

. "$SCRIPT_DIR/__run_setup_env__.bash"

# Install Python
pyenv install -s 3.5.2
pyenv virtualenv 3.5.2 EDBU_venv
pyenv activate EDBU_venv
pip install -r "$EDBU_DIR/shell/requirements.txt"

# Create Loggin Directory
mkdir -p "$EDBU_DIR/log/"

# Install Crontab
EDBU_JOB="*/15 * * * * bash \"$EDBU_DIR/shell/run.bash\" >> \"$EDBU_DIR/log/EDBU.log\" 2>&1"

IS_JOB_ADDED=`crontab -l 2>&1 | grep "15.*shell/run.bash"`

if [ -z "$IS_JOB_ADDED" ]; then
  (crontab -l 2>/dev/null; echo "$EDBU_JOB") | crontab -
else
  echo "crontab job is already added"
fi

